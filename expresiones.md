# Expresion NIT 
´´´
NIT:\s?[0-9]{1,10}
´´´

# Expresion Autorización 
´´´
(Autorización|AUTORIZACIÓN):\s?[0-9A-Z]{1,14}
´´´

# Expresion Monto 
´´´
Bs\.?\s?[0-9]{1,3}
´´´

# Expresion Numero Factura 
´´´
(^[0-9]{7,9}$|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})
´´´