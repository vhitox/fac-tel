#!/bin/bash

for item in $(ls facturas); do
echo -e "\n $item ----------"
# NIT
nit=$(cat facturas/$item | grep -oE 'NIT:\s?[0-9]{1,10}' | sed -E 's/NIT:\s?//g')
# NRO FACTURA
factura=$(cat facturas/$item | grep -oE '(^[0-9]{7,9}$|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})' | sed -E 's/\.//g')
# COD AUTORIZACION
cuf=$(cat facturas/$item | grep -oE '(Autorización|AUTORIZACIÓN):\s?[0-9A-Z]{1,14}' | sed -E 's/(Autorización|AUTORIZACIÓN):\s?//g')
# IMPORTE
importe=$(cat facturas/$item | grep -oE 'Bs\.?\s?[0-9]{1,3}' | sed -E 's/Bs\.?\s//g')
cadena="$item,$nit,$factura,$cuf,,$importe"
echo $cadena
echo $cadena >> facturas.csv

done
